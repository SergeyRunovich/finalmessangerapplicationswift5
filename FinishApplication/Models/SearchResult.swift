//
//  SearchResult.swift
//  FinishApplication
//
//  Created by Sergey Runovich on 5.04.21.
//

import Foundation
struct SearchResult {
    let name: String
    let email: String
}
