//
//  SettingsTableViewCell.swift
//  Messenger
//
//  Created by Sergey Runovich on 10.04.21.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    //MARK: - Properties -
    static let identifier = "firstCell"
    static let nib = "SettingsTableViewCell"
    
    
    //MARK: - IBOutlets -
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var nameCell: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
