//
//  ProfileViewController.swift
//  FinishApplication
//
//  Created by Сергей Рунович on 16.03.21.
//

import UIKit
import FirebaseAuth
import SDWebImage


enum ProfileViewModelType {
    case info
}
struct ProfileViewModel {
    let viewModelType: ProfileViewModelType
    let title: String
    let handler: (() -> Void)?
}



final class ProfileViewController: UIViewController {
    
    
    class func create() -> UIViewController? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let cotroller = storyboard.instantiateViewController(identifier: "profileVC") as! ProfileViewController
        return cotroller
        
    }
    
    
    
    var secondSection = [( icon: UIImage(named: "Legal"), title: "About"),
                         ( icon: UIImage(named: "Logout"), title: "Log Out")]
    
    
    func logOut() {
        let actionSheet = UIAlertController(title: "",
                                            message: "",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Log out", style: .destructive, handler: { _ in
            
            UserDefaults.standard.setValue(nil, forKey: "email")
            UserDefaults.standard.setValue(nil, forKey: "name")
            UserDefaults.standard.setValue(nil, forKey: "isLogin")
            
            do {
                try FirebaseAuth.Auth.auth().signOut()
                
                let vc = LoginViewController.create() ?? UIViewController()
                let nav = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true)
                
            }
            catch {
                print("Failed to log out")
            }
            
        }))
        
        
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel,
                                            handler: nil))
        
        self.present(actionSheet, animated: true)
    }
    
    @IBOutlet var tableView: UITableView!
    
    var data = [ProfileViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ProfileTableViewCell.self,
                           forCellReuseIdentifier: ProfileTableViewCell.identifier)
        
        data.append(ProfileViewModel(viewModelType: .info,
                                     title: "Name: \(UserDefaults.standard.value(forKey:"name") as? String ?? "No Name")",
                                     handler: nil))
        data.append(ProfileViewModel(viewModelType: .info,
                                     title: "Email: \(UserDefaults.standard.value(forKey:"email") as? String ?? "No Email")",
                                     handler: nil))
        
        
        
        
        
        let nib = UINib(nibName: SettingsTableViewCell.nib, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: SettingsTableViewCell.identifier)
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = createTableHeader()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .black
    }
    func createTableHeader() -> UIView? {
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else {
            return nil
        }
        
        let safeEmail = DatabaseManager.safeEmail(emailAddress: email)
        let filename = safeEmail + "_profile_picture.png"// было "_profile_picture.png"
        let path = "picture/"+filename
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width , height: 150))
        
        
        headerView.backgroundColor = .black
        
        let imageViewProfile = UIImageView(frame: CGRect(x: (headerView.frame.width-100) / 2, y: 20, width: 100, height: 100))
        imageViewProfile.contentMode = .scaleAspectFill
        imageViewProfile.layer.borderColor = UIColor.white.cgColor
        imageViewProfile.layer.masksToBounds = true
        imageViewProfile.layer.borderWidth = 3
        imageViewProfile.backgroundColor = .white
        imageViewProfile.layer.cornerRadius = imageViewProfile.frame.width / 2
        headerView.addSubview(imageViewProfile)
        
        StorageManager.shared.downloadURL(for: path, completion: { [weak self] result in
            switch result {
            case .success(let url):
                imageViewProfile.sd_setImage(with: url, completed: nil)
                print("hrllo")
            case .failure(let error):
                print("Failed to get download url:\(error)")
                imageViewProfile.image = UIImage(named: "white")
            }
            
        })
        
        return headerView
        
    }
    func downloadImage(imageViewProfile: UIImageView , url: URL) {
        URLSession.shared.dataTask(with: url, completionHandler: {data, _, error in
            guard let data = data,  error == nil else {
                return
            }
            
            
            DispatchQueue.main.async {
                let imagees = UIImage(data: data)
                imageViewProfile.image = imagees
                
            }
            
        }).resume()
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1:
            return 20
        
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return UIView()
        case 1:
            return UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.height, height: 100))
        default:
            return UIView()
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return data.count
        case 1:
            return secondSection.count
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        switch indexPath.section {
        case 0:
            let viewModel = data[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.identifier,for: indexPath) as! ProfileTableViewCell
            cell.setUp(with: viewModel)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.identifier, for: indexPath) as! SettingsTableViewCell
            cell.imageCell.image = secondSection[indexPath.row].icon
            cell.nameCell.text = secondSection[indexPath.row].title
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func about() {
        let urlComponets = URLComponents(string: "https://ktonanovenkogo.ru/voprosy-i-otvety/messendzher-chto-ehto-takoe-prostymi-slovami.html")!
        UIApplication.shared.open(urlComponets.url!)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            print("2")
        case 1:
            let data = secondSection[indexPath.row]
            if data.title == "Log Out" {
                logOut()
            }
            if data.title == "About" {
                about()
            }
        default:
            print("44")
        }
    }
}

class ProfileTableViewCell: UITableViewCell {
    
    static let identifier = "ProfileTableViewCell"
    
    public func setUp(with viewModel: ProfileViewModel) {
        self.textLabel?.text = viewModel.title
        switch viewModel.viewModelType {
        case .info:
            textLabel?.textAlignment = .left
            selectionStyle = .none
            textLabel?.textColor = .white
            self.backgroundColor = .black
        }
    }
    
}












