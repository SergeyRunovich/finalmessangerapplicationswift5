//
//  ConversationTableViewCell.swift
//  FinishApplication
//
//  Created by Sergey Runovich on 27.03.21.
//

import UIKit
import SDWebImage

class ConversationTableViewCell: UITableViewCell {
    //MARK: - Properties -
    static let identifier = "ConversationTableViewCell"
    
    
    private let userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = .white
        return imageView
    }()
    
    
    private let userNameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 21, weight: .semibold)
        label.textColor = .white
        return label
        
    }()
    
    private let userMessageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .regular)
        label.numberOfLines = 0
        label.textColor = .lightGray
        return label
    }()
    private let timeMessageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20, weight: .regular)
        label.numberOfLines = 0
        label.textColor = .lightGray
        return label
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(userImageView )
        contentView.addSubview(userNameLabel )
        contentView.addSubview(userMessageLabel )
        contentView.addSubview(timeMessageLabel)
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        userImageView.frame = CGRect(x: 10,
                                     y: 10,
                                     width: 70,
                                     height: 70)
        userImageView.layer.cornerRadius = userImageView.frame.width / 2
        userNameLabel.frame = CGRect(x: userImageView.right + 10,
                                     y: 10,
                                     width: contentView.width - 20 - userImageView.width,
                                     height: (contentView.height - 20)/2)
        
        userMessageLabel.frame = CGRect(x: userImageView.right + 10,
                                        y: userNameLabel.bottom + 10,
                                        width: contentView.width - 10 - userImageView.width,
                                        height: (contentView.height - 80))
        timeMessageLabel.frame = CGRect(x: contentView.width - 80 , y: 40, width: contentView.width, height: 15)
    }
    
    public func configure(with model: Conversation) {
        self.userMessageLabel.text = model.latestMessage.text
        self.userNameLabel.text = model.name
        self.timeMessageLabel.text = model.latestMessage.date
        
        let path = "picture/\(model.otherUserEmail)_profile_picture.png"
        StorageManager.shared.downloadURL(for: path, completion: { [weak self] result in
            switch result {
            case  .success(let url):
                
                DispatchQueue.main.async {
                    self?.userImageView.sd_setImage(with: url, completed: nil)
                }
                
            case .failure(let error):
                
                print("Failed to get imagee url: \(error)")
            }
        })
    }
}
