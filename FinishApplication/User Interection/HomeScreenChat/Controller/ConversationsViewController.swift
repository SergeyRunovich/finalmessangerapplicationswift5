//
//  ViewController.swift
//  FinishApplication
//
//  Created by Сергей Рунович on 16.03.21.
//

import UIKit
import FirebaseAuth
import JGProgressHUD




class ConversationsViewController: UIViewController, UISearchResultsUpdating {
    
    //MARK: - Properties -
    var arrayIndexPath: [IndexPath] = []
    private let spinner = JGProgressHUD(style: .dark)
    
    private var conversations = [Conversation]()
    private var searchUsersData: [Conversation] = []
    var searchBar: UISearchBar?
    var searchContactsController: UISearchController?
    
    
    private let noConversationsLabel: UILabel = {
        let label = UILabel()
        label.text = "No Conversation!"
        label.textAlignment = .center
        label.textColor = .gray
        label.font = .systemFont(ofSize: 21, weight: .medium)
        label.isHidden = true
        return label
    }()
    
    let userDefaults = UserDefaults()
    
    
    
    
    
    
    
    
    
    
    class func create() -> UIViewController? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let cotroller = storyboard.instantiateViewController(identifier: "converVC") as! ConversationsViewController
        return cotroller
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .black
        let data = userDefaults.string(forKey: "profile_picture_url")
        setupSearchController()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(didTapComposeButton))
        //navigationItem.leftBarButtonItem = editButtonItem
        view.addSubview(tableView)
        view.addSubview(noConversationsLabel)
        setupTableView()
        self.view.backgroundColor = .black
        
    }
    
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.isHidden = true
            tableView.register(ConversationTableViewCell.self, forCellReuseIdentifier: ConversationTableViewCell.identifier)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        validateAuth()
        startListeningForConversation()
    }
    
    func setupSearchController() {
        if #available(iOS 11.0, *) {
            searchContactsController = UISearchController(searchResultsController: nil)
            searchContactsController?.searchResultsUpdater = self
            searchContactsController?.obscuresBackgroundDuringPresentation = false
            searchContactsController?.searchBar.delegate = self
            navigationItem.searchController = searchContactsController
        } else {
            searchBar = UISearchBar()
            searchBar?.delegate = self
            searchBar?.placeholder = "Search"
            searchBar?.searchBarStyle = .minimal
            searchBar?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
            tableView.tableHeaderView = searchBar
        }
    }
    
    
    private func startListeningForConversation() {
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else {
            return
        }
        print("starting conversation fetch...")
        let safeEmail = DatabaseManager.safeEmail(emailAddress: email)
        DatabaseManager.shared.getAllConversation(for: safeEmail, complition: { [ weak self ] result in
            switch result {
            case .success(let conversations):
                print("successfully got conversation models")
                guard !conversations.isEmpty else {
                    self?.tableView.isHidden = true
                    self?.noConversationsLabel.isHidden = false
                    return
                }
                self?.noConversationsLabel.isHidden = true
                self?.tableView.isHidden = false
                self?.conversations = conversations
                print("Hello: \(conversations)")
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            case .failure(let error):
                self?.tableView.isHidden = true
                self?.noConversationsLabel.isHidden = false
                print("failed to get convos: \(error)")
            }
        })
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    @objc private func didTapComposeButton() {
        let vc = NewConversationViewController()
        vc.completion = { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            let currentConversation = strongSelf.conversations
            if let targetConversation = currentConversation.first(where: {
                $0.otherUserEmail == DatabaseManager.safeEmail(emailAddress: result.email)
            }) {
                let vc = ChatViewController(with: targetConversation.otherUserEmail, id: targetConversation.id)
                vc.isNewConversation = true
                vc.title = targetConversation.name
                vc.navigationItem.largeTitleDisplayMode = .never
                strongSelf.navigationController?.pushViewController(vc, animated: true)
            } else {
                strongSelf.createNewConversation(result: result)
            }
        }
        let navVC = UINavigationController(rootViewController: vc)
        present(navVC, animated: true)
        
    }
    
    
    private func createNewConversation(result: SearchResult) {
        let name = result.name
        let email = DatabaseManager.safeEmail(emailAddress: result.email)
        // check in datbase if conversation with these two users exists
        // if it does, reuse conversation id
        // otherwise use existing code
        
        DatabaseManager.shared.conversationExists(iwth: email, completion: { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let conversationId):
                let vc = ChatViewController(with: email, id: conversationId)
                vc.isNewConversation = false
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                strongSelf.navigationController?.pushViewController(vc, animated: true)
            case .failure(_):
                let vc = ChatViewController(with: email, id: nil)
                vc.isNewConversation = true
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                strongSelf.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = self.view.bounds
        
        
        noConversationsLabel.frame = CGRect(x: 10,
                                            y: (view.height-100)/2,
                                            width: view.width-20,
                                            height: 100)
    }
    private func validateAuth() {
        let userLogin = userDefaults.bool(forKey: "isLogin")
        //if FirebaseAuth.Auth.auth().currentUser == nil
        if !userLogin {
            let vc = LoginViewController.create() ?? UIViewController()
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            present(nav, animated: true)
        }
    }
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    
    
    
}


extension ConversationsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let swipeMute = UIContextualAction(style: .normal, title: "Mute", handler: {action , view , success in
            print("ok")
        })
        swipeMute.backgroundColor = .orange
        swipeMute.image = #imageLiteral(resourceName: "mute1")
        
        
        let swipeDelete = UIContextualAction(style: .destructive, title: "Delete", handler: {action , view , success in
            let conId = self.conversations[indexPath.row].id
            tableView.beginUpdates()
            self.conversations.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
            print("kk")
            DatabaseManager.shared.deleteConversation(conversationId: conId, completion: { success in
                if !success {
                    // add model and row back and show error alert
                    
                }
            })
            
            tableView.endUpdates()
        })
        
        swipeDelete.image = #imageLiteral(resourceName: "basket5")
        
        
        let swipeArhive = UIContextualAction(style: .normal, title: "Arhive", handler: {action , view , success in
            print("ok")
        })
        
        swipeArhive.image = #imageLiteral(resourceName: "archive")
        
        let conv = UISwipeActionsConfiguration(actions: [swipeMute, swipeDelete,  swipeArhive])
        conv.performsFirstActionWithFullSwipe = false
        
        return UISwipeActionsConfiguration(actions: [swipeArhive, swipeDelete, swipeMute ])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchUsersData.isEmpty {
            print(searchUsersData)
            return conversations.count
        } else {
            return searchUsersData.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = conversations[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ConversationTableViewCell.identifier, for: indexPath) as! ConversationTableViewCell
       
        if searchUsersData.isEmpty {
            cell.configure(with: model)
        } else {
            let searchModel = searchUsersData[indexPath.row]
            cell.configure(with: searchModel)
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated:  true)
        
        
        let model = conversations[indexPath.row]
        if searchUsersData.isEmpty {
            openConversation(model)
        } else {
            let searchModel = searchUsersData[indexPath.row]
            openConversation(searchModel)
        }
        searchBar?.resignFirstResponder()
        searchContactsController?.searchBar.resignFirstResponder()
    }
    
    
    
    func openConversation(_ model: Conversation) {
        let vc = ChatViewController(with: model.otherUserEmail, id: model.id)
        vc.title = model.name
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension ConversationsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchText = searchBar.text?.first?.lowercased() else  { return }
        
        if let found = conversations.first(where: {$0.name.first?.lowercased() == searchText}) {
            searchUsersData = []
            searchUsersData.append(found)
            tableView.reloadData()
        } else {
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchUsersData = []
        tableView.reloadData()
    }
    
    
}
