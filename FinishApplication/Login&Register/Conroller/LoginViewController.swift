//
//  LoginViewController.swift
//  FinishApplication
//
//  Created by Сергей Рунович on 16.03.21.
//

import UIKit
import FirebaseAuth
import RealmSwift
import JGProgressHUD
import SDWebImage


class LoginViewController: UIViewController {
    //MARK: - Properties -
    let registerButton = UIBarButtonItem()
    let userDefaults = UserDefaults.standard
    private let spinner = JGProgressHUD(style: .dark )
    
    
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    
    
    
    
    class func create() -> UIViewController? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let cotroller = storyboard.instantiateViewController(identifier: "loginVC") as! LoginViewController
        return cotroller
        
    }
    
    
    //MARK: - LifeCycle -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = "Log In"
        view.backgroundColor = .systemBackground
        addButton()
        seetingField()
        emailField?.delegate = self
        passwordField?.delegate = self
        view.backgroundColor = .lightGray
        loginButton?.addTarget(self, action: #selector(loginButtonTaped), for: .touchUpInside)
        //self.view.backgroundColor = .black
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    private func saveLogin() {
        
        userDefaults.setValue(true, forKey: "isLogin")
        
    }
    
    private func addButton() {
        registerButton.action = #selector(register)
        registerButton.style = .plain
        registerButton.title = "Register"
        registerButton.target = self
        navigationItem.rightBarButtonItem = registerButton
    }
    
    
    @objc func register() {
        let main: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = main.instantiateViewController(withIdentifier: "secVC")  as! RegisterViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    func seetingField(){
        emailField?.layer.cornerRadius = 12
        emailField?.layer.borderWidth = 1
        emailField?.returnKeyType = .continue
        emailField?.autocorrectionType = .no
        emailField?.autocapitalizationType = .none
        emailField?.layer.borderColor = UIColor.lightGray.cgColor
        emailField?.attributedPlaceholder = NSAttributedString(string: "  Email adress....",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        passwordField?.layer.cornerRadius = 12
        passwordField?.layer.borderWidth = 1
        passwordField?.returnKeyType = .continue
        passwordField?.layer.borderColor = UIColor.lightGray.cgColor
        passwordField?.isSecureTextEntry = true
        passwordField?.attributedPlaceholder = NSAttributedString(string: "  Password....",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        loginButton?.layer.cornerRadius = 12
        loginButton?.backgroundColor = .link
    }
    
    
    
    
    @objc private func  loginButtonTaped() {
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        guard let email = emailField.text, let password = passwordField.text,
              !email.isEmpty , !password.isEmpty , password.count >= 6  else {
            alertUserLoginError()
            return
        }
        
        spinner.show(in: view)
      
        
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password, completion:  {  [weak self] authResult , error in
            guard let strongSelf  = self else {
                return
            }
            
            DispatchQueue.main.async {
                strongSelf.spinner.dismiss()
            }
            
            guard let result = authResult , error == nil else {
                print("Failed to log in user with email: \(email)")
                return
            }
            let user = result.user
            let safeEmail = DatabaseManager.safeEmail(emailAddress: email)
            DatabaseManager.shared.getDataFor(path: safeEmail, complition: { [weak self] result in
                switch result {
                case .success(let data):
                    guard let userData = data as? [String: Any],
                          let firstName = userData["first_name"] as? String,
                          let lastName = userData["last_name"] as? String else {
                        return
                    }
                    UserDefaults.standard.set("\(firstName) \(lastName)", forKey: "name")
                case .failure(let error):
                    print("Failed to read data with error \(error)")
                }
            })
            UserDefaults.standard.set(email, forKey: "email")
            
            
            
            
            print("Logged In User: \(user)")
            let vc = ConversationsViewController()
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .flipHorizontal
            self?.saveLogin()
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
            
        })
    }
    
    
    
    
    
    func alertUserLoginError() {
        let alert = UIAlertController(title: "Woops",
                                      message: "Please enter all information to log in.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss",
                                      style: .cancel,
                                      handler: nil))
        present(alert, animated: true)
    }
    
}


extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            loginButtonTaped()
        }
        return true
    }
}

