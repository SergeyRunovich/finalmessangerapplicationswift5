//
//  RegisterViewController.swift
//  FinishApplication
//
//  Created by Сергей Рунович on 16.03.21.
//

import UIKit
import FirebaseAuth
import JGProgressHUD
import SDWebImage
import FirebaseStorage

class RegisterViewController: UIViewController {
    //MARK: - Properties -
    let userDefaults = UserDefaults.standard
    private let spinner = JGProgressHUD(style: .dark )
    //MARK: - IBOutlets -
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var registrateButton: UIButton!
    
    
    //MARK: - LifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Log In"
        imageSetting()
        seetingField()
        firstNameField.delegate = self
        lastNameField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        registrateButton.addTarget(self, action: #selector(registerButtonTaped), for: .touchUpInside)
        
        imageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self,
                                             action: #selector(didTapChangeProfilePic))
        imageView.addGestureRecognizer(gesture)
    }
    
    
    func imageSetting() {
        imageView.layer.cornerRadius = imageView.frame.width / 2.5
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    private func saveLogin() {
        
        userDefaults.setValue(true, forKey: "isLogin")
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    @objc private func didTapChangeProfilePic() {
        presentPhotoActionShit()
    }
    func seetingField(){
        firstNameField.layer.cornerRadius = 12
        firstNameField.layer.borderWidth = 1
        firstNameField.returnKeyType = .continue
        firstNameField.layer.borderColor = UIColor.lightGray.cgColor
        firstNameField?.attributedPlaceholder = NSAttributedString(string: "  First Name.....",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        lastNameField.layer.cornerRadius = 12
        lastNameField.layer.borderWidth = 1
        lastNameField.returnKeyType = .continue
        lastNameField.layer.borderColor = UIColor.lightGray.cgColor
        lastNameField?.attributedPlaceholder = NSAttributedString(string: "  Last Name",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        emailField.layer.cornerRadius = 12
        emailField.layer.borderWidth = 1
        emailField.returnKeyType = .continue
        emailField.autocorrectionType = .no
        emailField.autocapitalizationType = .none
        emailField.layer.borderColor = UIColor.lightGray.cgColor
        emailField?.attributedPlaceholder = NSAttributedString(string: "  Email adress....",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        passwordField.layer.cornerRadius = 12
        passwordField.layer.borderWidth = 1
        passwordField.returnKeyType = .continue
        passwordField.layer.borderColor = UIColor.lightGray.cgColor
        passwordField.isSecureTextEntry = true
        passwordField?.attributedPlaceholder = NSAttributedString(string: "  Password....",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        registrateButton.layer.cornerRadius = 12
        registrateButton.backgroundColor = .link
        passwordField.autocorrectionType = .no
        passwordField.textContentType = .oneTimeCode
        
    }
    
    @objc private func  registerButtonTaped() {
        firstNameField.resignFirstResponder()
        lastNameField.resignFirstResponder()
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        guard let email = emailField.text,
              
              let password = passwordField.text,
              let firstName = firstNameField.text,
              let lastName = lastNameField.text,
              !email.isEmpty,
              !password.isEmpty,
              !firstName.isEmpty,
              !lastName.isEmpty,
              password.count >= 6  else {
            alertUserLoginError()
            return
        }
        
        UserDefaults.standard.set("\(firstName) \(lastName)", forKey: "name")
        UserDefaults.standard.set(email, forKey: "email")
        
        
        
        spinner.show(in: view)
        
        
        DatabaseManager.shared.userExists(with: email, completion: { [weak self] exists in
            guard let strongSelf = self else {
                return
            }
            
            DispatchQueue.main.async {
                self?.spinner.dismiss()
            }
            
            guard !exists else {
                strongSelf.alertUserLoginError(message: "Looks like a user account for that email address already exists.")
                print("registrate ok")
                return
            }
            
            FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password, completion: {  authResult, error in
                
                guard authResult != nil , error == nil else {
                    print("Error cureating user")
                    return
                }
                UserDefaults.standard.setValue(email, forKey: "email")
                UserDefaults.standard.setValue("\(firstName) \(lastName)", forKey: "name")
                
                let chatUser = ChatAppUser(firstName: firstName,
                                           lastname: lastName,
                                           emailAddress: email)
                DatabaseManager.shared.insertUser(with: chatUser , completion: {success in
                    if success {
                        //upload image
                        guard let image = self?.imageView.image,
                              let data = image.pngData() else {
                            return
                        }
                        let fileName = chatUser.profilePictureFileName
                        StorageManager.shared.uploadProfilePicture(with: data,fileName: fileName,completion: { result in
                            switch result {
                            case.success(let downloadUrl):
                                UserDefaults.standard.set(downloadUrl, forKey: "profile_picture_url")
                                print("romaebettelokvavgyste\(downloadUrl)")
                            case .failure(let error):
                                print("Storage manager error: \(error)")
                            }
                        })
                        
                    }
                })
                
                
                
                
                let vc = ConversationsViewController()
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .flipHorizontal
                self?.saveLogin()
                strongSelf.navigationController?.dismiss(animated: true, completion: nil)
            })
            
        })
    }
    
    
    
    func alertUserLoginError(message: String = "Please enter all information to create a new account.") {
        let alert = UIAlertController(title: "Woops",
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss",
                                      style: .cancel,
                                      handler: nil))
        present(alert, animated: true)
    }
    
}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            registerButtonTaped()
        }
        else if textField == firstNameField {
            lastNameField.becomeFirstResponder()
        }
        else if textField == lastNameField{
            emailField.becomeFirstResponder()
        }
        return true
    }
}

extension RegisterViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func presentPhotoActionShit() {
        let actionSheet = UIAlertController(title: "Profile",
                                            message: "How would you like to select a picture",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel,
                                            handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Take Photo",
                                            style: .default,
                                            handler: { [weak self] _ in
                                                
                                                self?.presentCamera()
                                                
                                            }))
        actionSheet.addAction(UIAlertAction(title: "Chose Photo",
                                            style: .default,
                                            handler: { [weak self] _ in
                                                self?.presentPhotoPicker()
                                                
                                            }))
        present(actionSheet, animated: true)
    }
    
    func presentCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    func presentPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        
        self.imageView.image = selectedImage
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
}
