# Messenger Real Time Chat App #


### Features: ###

* Email/Pass Registration / Log In
* Photo Messages
* Video Messages
* Real Time Conversations
* Location Messages
* Search for Users
* Deleting Conversations
* User Profile
* Dark Mode Support



### Screenshots ###




![Scheme](https://bitbucket.org/repo/r9bEX6j/images/1498320769-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-14%20%D0%B2%2022.19.16.png)
![Scheme](https://bitbucket.org/repo/z9qjRLn/images/1190744695-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-21%20%D0%B2%2021.50.05.png)
![Scheme](https://bitbucket.org/repo/z9qjRLn/images/2171619944-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-21%20%D0%B2%2021.50.11.png)
![Scheme](https://bitbucket.org/repo/z9qjRLn/images/3668807353-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-21%20%D0%B2%2022.05.14.png)


### Libraries ###

* Firebase
* JGProgressHUD
* SDWebImage
* MessageKit
